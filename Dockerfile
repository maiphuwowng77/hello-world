# use the latest Node.js image
FROM node:16

# set the working directory of the container 
WORKDIR /app

# copy package.json file into the container
COPY package.json ./

# install dependencies specified in package.json
RUN npm install

# copy source code
COPY . .

# expose port 8080 outside the container
EXPOSE 8080

# Run the server when the container starts
CMD ["node", "src/index.js"]